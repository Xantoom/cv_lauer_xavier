#include <stdio.h>
#include <stdlib.h>
#include "town.h"
#include "road.h"

struct town *createTown(char *name) {
  struct town *E = (struct town *)calloc(1, sizeof(struct town));
  E->name = name;
  E->alist = NULL;
  return E;
}

void freeTown(struct town *T) {
  free(T);
}

char *getTownName(struct town *T) {
  return T->name;
}

struct list *getAList(struct town *T) {
  return T->alist;
}

void viewTown(struct town *T) {
  printTownName(T);
  printf("Relié à: ");
  printf("[");
  struct list *E = (struct list *)calloc(1, sizeof(struct list));
  E = T->alist;
  while(E->head != NULL) {
    printf("%s, ", E->head->data->V->name);
    E->head = E->head->suc;
  }
  free(E);
  printf("]\n\n");
}

void printTownName(struct town *T) {
  printf("\n\nVille: %s\n\n", T->name);
}

struct list *readTown(char *fileName) {
  FILE* file = fopen(fileName, "r");
  if(file == NULL) {
    printf("Impossible d'ouvrir le fichier %s. Vérifier qu'il existe, ou qu'il est au bon endroit.", fileName);
  }else{
    struct list *E = (struct list *)calloc(1, sizeof(struct list));
    fscanf("%d", E->numelm);
    char tabVilles[E->numelm - 1], int i;
    for(i = 0; i < E->numelm; i++) {
      fscanf("%s", &tabVilles[i]);
    }
    struct elmlist tabELM[E->numelm - 1];
    for(i = 0; i < E->numelm; i++) {
      struct elmlist *ELM = (struct elmlist *)calloc(1, sizeof(struct elmlist));
      ELM->data = tabVilles[i];
      tabELM[i] = ELM;
    }
    for(i = 0; i < E->numelm; i++) {
      if(i == 0) {
        tabELM[i]->pred = NULL;
        tabELM[i]->suc = tabELM[i+1];
        E->head = tabELM[i];
      } else if(i == E->numelm - 1) {
        tabELM[i]->pred = tabELM[i-1];
        tabELM[i]->suc = NULL;
        E->tail = tabELM[i];
      } else {
        tabELM[i]->pred = tabELM[i-1];
        tabELM[i]->suc = tabELM[i+1];
      }
    }
    fclose(file);
    return E;
  }
}