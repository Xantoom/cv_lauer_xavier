#include <stdlib.h>
#include "road.h"
#include "town.h"

struct road *createRoad(struct town *U, struct town *V) {
	struct road *R = (struct road *)calloc(1, sizeof(struct road));
	R->U = U;
	R->V = V;
	return R;
}

void freeRoad(struct road *R) {
	free(R);
}

struct town *getURoad(struct road *R) {
	return R->U;
}

void setURoad(struct road *R, struct town *T) {
	R->U = T;
}

struct town *getVRoad(struct road *R) {
	return R->V;
}

void setVRoad(struct road *R, struct town *T) {
	R->V = T;
}

void viewRoad(struct road *R) {
	printf("\nRoute entre %s et %s.\n\n", R->U->name, R->V->name);
}

struct list *readRoad(char *fileName) {
  FILE* file = fopen(fileName, "r");
  if(file == NULL) {
    printf("Impossible d'ouvrir le fichier %s. Vérifier qu'il existe, ou qu'il est au bon endroit.", fileName);
  }else{
    struct list *E = (struct list *)calloc(1, sizeof(struct list));
    fscanf("%d", E->numelm);
    char tabRoutes[E->numelm - 1], int i;
    for(i = 0; i < E->numelm; i++) {
      fscanf("%s", &tabRoutes[i]);
    }
    struct elmlist tabELM[E->numelm - 1];
    for(i = 0; i < E->numelm; i++) {
      struct elmlist *ELM = (struct elmlist *)calloc(1, sizeof(struct elmlist));
      ELM->data = tabRoutes[i];
      tabELM[i] = ELM;
    }
    for(i = 0; i < E->numelm; i++) {
      if(i == 0) {
        tabELM[i]->pred = NULL;
        tabELM[i]->suc = tabELM[i+1];
        E->head = tabELM[i];
      } else if(i == E->numelm - 1) {
        tabELM[i]->pred = tabELM[i-1];
        tabELM[i]->suc = NULL;
        E->tail = tabELM[i];
      } else {
        tabELM[i]->pred = tabELM[i-1];
        tabELM[i]->suc = tabELM[i+1];
      }
    }
    fclose(file);
    return E;
  }
}