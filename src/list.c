#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "list.h"
#include "town.h"
#include "road.h"

struct list *new() {
  return NULL;
}

bool isempty(struct list *L) {
  return L == new();
}

void dellist(struct list *L, void (*ptrF)()) {
  if(ptrF == NULL) { // ne supprime pas les données
    while(L->head != NULL) {
      struct elmlist *supp = L->head;
      L->head = L->head->suc;
      free(supp);
    }
    /*for(struct list *iterator = L; iterator; iterator = iterator->suc) {
      free(iterator);
    }*/
  } else { // suppression complète
    while(L->head != NULL) {
      struct elmlist *supp = L->head;
      L->head = L->head->suc;
      free(supp->data);
      free(supp);
    }
    /*for(struct list *iterator = L; iterator; iterator = iterator->suc) {
      free(iterator->data);
      free(iterator);
    }*/
  }
  L->tail = NULL;
  L->numelm = 0;
  free(L);
}

void viewlist(struct list *L, void (*ptrF)()) {
  printf("[");
  struct list *E = (struct list *)calloc(1, sizeof(struct list));
  E = L;
  while(E->head != NULL) {
    printf("%p\n", &E->head->data);
    E->head = E->head->suc;
  }
  /*for(struct list *iterator = L; iterator; iterator = iterator->suc) {
    printf("%d, ", iterator->data);
  }*/
  printf("]\n\n");
}

void cons(struct list *L, void *data) {
  struct elmlist *E = (struct elmlist *)calloc(1, sizeof(struct elmlist));
  E->data = data;
  E->pred = NULL;
  E->suc = L->head;
  L->head->pred = E;
  L->numelm += 1;
  L->head = E;
}

void insert_after(struct list *L, void *data, struct elmlist *ptrelm) {
  struct elmlist *E = (struct elmlist *)calloc(1, sizeof(struct elmlist));
  E->data = data;
  E->pred = ptrelm;
  E->suc = ptrelm->suc;
  ptrelm->suc = E;
  L->numelm += 1;
}

void insert_ordered(struct list *L, void *data, struct town *departure) {
 if(L->head == NULL) {
   struct elmlist *E = (struct elmlist *)calloc(1, sizeof(struct elmlist));
   E->data = data;
   E->pred = NULL;
   L->head = E;
 } else {
    struct elmlist *iterator = L->head;

    if(departure == NULL) { // C'est la liste des villes
      char *name = (char *) data;
    } else { // C'est une liste d'ajacence, une liste de routes
      SetURoad(departure);
    }

    if(iterator == NULL) { // Ajout en queue

    } else if(iterator == L->head) { // Ajout en Tête
      
    } else {
      
    }
  }
}
